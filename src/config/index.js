import Vue from 'vue'
// 导入需要的组件
import {
  Pagination,
  // Dialog,
  // Autocomplete,
  // Dropdown,
  // DropdownMenu,
  // DropdownItem,
  // Menu,
  // Submenu,
  // MenuItem,
  // MenuItemGroup,
  // Input,
  // InputNumber,
  // Radio,
  //   RadioGroup,
  //   RadioButton,
  //   Checkbox,
  //   CheckboxButton,
  //   CheckboxGroup,
  //   Switch,
  Select,
  Option,
  OptionGroup,
  Button,
  //   ButtonGroup,
  //   Table,
  //   TableColumn,
  //   DatePicker,
  //   TimeSelect,
  //   TimePicker,
  //   Popover,
  //   Tooltip,
  //   Breadcrumb,
  //   BreadcrumbItem,
  //   Form,
  //   FormItem,
  //   Tabs,
  //   TabPane,
  //   Tag,
  //   Tree,
  //   Alert,
  //   Slider,
  //   Icon,
  //   Row,
  //   Col,
  //   Upload,
  //   Progress,
  //   Spinner,
  //   Badge,
  //   Card,
  //   Rate,
  //   Steps,
  //   Step,
  Carousel,
  CarouselItem,
  Collapse,
  CollapseItem,
  //   Cascader,
  //   ColorPicker,
  //   Transfer,
  //   Container,
  //   Header,
  //   Aside,
  //   Main,
  //   Footer,
  //   Timeline,
  //   TimelineItem,
  //   Link,
  Divider,
  Image,
  //   Calendar,
  //   Backtop,
  //   PageHeader,
  //   CascaderPanel,
  Loading,
  MessageBox,
  Message
//   Notification
} from 'element-ui'

// 这里放注册组件
let useObj = {
  Button,
  directive: Loading.directive,
  Select,
  Option,
  OptionGroup,
  Pagination,
  Carousel,
  CarouselItem,
  Collapse,
  CollapseItem,
  Divider,
  Image
}
// 这里放挂载方法
let prototypeObj = {
  MessageBox,
  Alert: MessageBox.alert,
  Message
}

function VueComponent () {
  console.log(useObj)
  for (let x in useObj) {
    Vue.use(useObj[x])
  }
}
function VuePrototype () {
  for (let x in prototypeObj) {
    // 将首字母转化为小写
    let str = x.slice(0, 1).toLowerCase() + x.slice(1)
    // 挂载到Vue原型上
    Vue.prototype[`$${str}`] = prototypeObj[x]
  }
}

// 导出方法
export function elementFunction () {
  VueComponent()
  VuePrototype()
}
