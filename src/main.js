// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import api from './api/index.js'
import {elementFunction} from './config/index.js'
import { Avatar, Card, Container, Descriptions, DescriptionsItem, Dialog, Header, Main, Tag } from 'element-ui'
Vue.use(elementFunction)
Vue.use(Container)
Vue.use(Card)
Vue.use(Descriptions)
Vue.use(DescriptionsItem)
Vue.use(DescriptionsItem)
Vue.use(Header)
Vue.use(Main)
Vue.use(Tag)
Vue.use(Avatar)
Vue.use(Dialog)

Vue.prototype.$api = api
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
